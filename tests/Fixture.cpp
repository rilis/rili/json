#include <fstream>
#include <rili/JSON.hpp>
#include <string>
#include <tests/Fixture.hpp>

void Fixture::before() {
    {
        std::ifstream f(std::string(RILI_JSON_SOURCE_TREE_PATH) + "/tests/fixtures/" + fixtureName() + "/" +
                        scenarioName() + ".json");
        m_inputJson = std::string((std::istreambuf_iterator<char>(f)), std::istreambuf_iterator<char>());
        EXPECT_FALSE(m_inputJson.empty());
    }
}

void Fixture::after() {
    if (!m_shouldFail) {
        {
            std::ifstream f(std::string(RILI_JSON_SOURCE_TREE_PATH) + "/tests/fixtures/" + fixtureName() + "/" +
                            scenarioName() + ".expected.json");
            m_expectedJson = std::string((std::istreambuf_iterator<char>(f)), std::istreambuf_iterator<char>());
            EXPECT_FALSE(m_expectedJson.empty());
        }
        rili::JSON j;
        j.parse(m_inputJson, m_maxAllowedDepth);
        std::string stringified = j.stringify();
        EXPECT_EQ(stringified, m_expectedJson);
        j.parse(stringified, m_maxAllowedDepth);
        EXPECT_EQ(stringified, j.stringify());
    } else {
        try {
            rili::JSON j;
            j.parse(m_inputJson, m_maxAllowedDepth);
            ADD_FAILURE("SyntaxErrror expected but not received any exception");
        } catch (rili::JSON::SyntaxError& e) {
            EXPECT_EQ(e.position(), m_exceptedFailurePosition);
        } catch (...) {
            ADD_FAILURE("SyntaxErrror expected but received other exception");
        }
    }
}

Fixture::Fixture()
    : rili::test::TestBaseFixture(), m_shouldFail(false), m_exceptedFailurePosition(0), m_maxAllowedDepth(20) {}

void Fixture::shouldFail(bool shouldFail, size_t position) {
    m_shouldFail = shouldFail;
    m_exceptedFailurePosition = position;
}

void Fixture::setMaxDepth(size_t maxDepth) { m_maxAllowedDepth = maxDepth; }

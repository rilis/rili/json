#include <tests/Fixture.hpp>

typedef Fixture json_checker;

TEST_F(json_checker, fail1) {}
TEST_F(json_checker, fail2) { shouldFail(true, 17); }
TEST_F(json_checker, fail3) { shouldFail(true, 1); }
TEST_F(json_checker, fail4) { shouldFail(true, 15); }
TEST_F(json_checker, fail5) { shouldFail(true, 22); }
TEST_F(json_checker, fail6) { shouldFail(true, 4); }
TEST_F(json_checker, fail7) { shouldFail(true, 25); }
TEST_F(json_checker, fail8) { shouldFail(true, 15); }
TEST_F(json_checker, fail9) { shouldFail(true, 21); }

TEST_F(json_checker, fail10) { shouldFail(true, 57); }
TEST_F(json_checker, fail11) { shouldFail(true, 25); }
TEST_F(json_checker, fail12) { shouldFail(true, 23); }
TEST_F(json_checker, fail13) { shouldFail(true, 39); }
TEST_F(json_checker, fail14) { shouldFail(true, 27); }
TEST_F(json_checker, fail15) { shouldFail(true, 58); }
TEST_F(json_checker, fail16) { shouldFail(true, 1); }
TEST_F(json_checker, fail17) { shouldFail(true, 58); }
TEST_F(json_checker, fail18) { shouldFail(true, 19); }
TEST_F(json_checker, fail19) { shouldFail(true, 21); }
TEST_F(json_checker, fail20) { shouldFail(true, 16); }
TEST_F(json_checker, fail21) { shouldFail(true, 25); }
TEST_F(json_checker, fail22) { shouldFail(true, 25); }
TEST_F(json_checker, fail23) { shouldFail(true, 14); }
TEST_F(json_checker, fail24) { shouldFail(true, 1); }
TEST_F(json_checker, fail25) { shouldFail(true, 27); }
TEST_F(json_checker, fail26) { shouldFail(true, 39); }
TEST_F(json_checker, fail27) { shouldFail(true, 16); }
TEST_F(json_checker, fail28) { shouldFail(true, 17); }
TEST_F(json_checker, fail29) { shouldFail(true, 1); }
TEST_F(json_checker, fail30) { shouldFail(true, 1); }
TEST_F(json_checker, fail31) { shouldFail(true, 1); }
TEST_F(json_checker, fail32) { shouldFail(true, 40); }
TEST_F(json_checker, fail33) { shouldFail(true, 11); }

TEST_F(json_checker, pass1) {}
TEST_F(json_checker, pass2) {}

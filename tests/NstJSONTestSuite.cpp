#include <tests/Fixture.hpp>

typedef Fixture nst_JSONTestSuite;

TEST_F(nst_JSONTestSuite, fail1) { shouldFail(true, 7); }
TEST_F(nst_JSONTestSuite, fail2) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail3) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail4) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail5) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail6) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail7) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail8) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail9) { shouldFail(true, 4); }

TEST_F(nst_JSONTestSuite, fail10) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail11) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail12) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, fail13) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail14) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, fail15) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail16) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail17) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail18) { shouldFail(true, 11); }
TEST_F(nst_JSONTestSuite, fail19) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail20) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail21) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail22) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail23) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail24) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail25) { shouldFail(true, 8); }
TEST_F(nst_JSONTestSuite, fail26) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail27) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail28) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail29) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail30) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail31) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail32) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail33) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail34) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail35) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail36) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail37) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail38) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail39) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail40) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail41) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail42) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail43) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail44) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail45) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail46) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail47) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail48) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail49) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail50) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail51) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail52) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail53) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail54) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail55) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail56) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail57) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail58) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail59) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail60) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail61) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, fail62) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, fail63) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail64) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail65) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, fail66) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail67) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail68) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail69) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail70) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail71) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail72) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail73) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail74) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail75) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail76) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail77) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail78) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail79) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail80) { shouldFail(true, 19); }
TEST_F(nst_JSONTestSuite, fail81) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail82) { shouldFail(true, 6); }
TEST_F(nst_JSONTestSuite, fail83) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail84) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail85) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail86) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail87) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, fail88) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail89) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail90) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail91) { shouldFail(true, 7); }
TEST_F(nst_JSONTestSuite, fail92) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail93) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail94) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail95) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail96) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, fail97) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail98) { shouldFail(true, 8); }
TEST_F(nst_JSONTestSuite, fail99) { shouldFail(true, 1); }

TEST_F(nst_JSONTestSuite, fail100) { shouldFail(true, 8); }
TEST_F(nst_JSONTestSuite, fail101) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, fail102) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, fail103) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, fail104) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, fail105) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, fail106) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail107) { shouldFail(true, 7); }
TEST_F(nst_JSONTestSuite, fail108) { shouldFail(true, 21); }
TEST_F(nst_JSONTestSuite, fail109) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, fail110) { shouldFail(true, 0); }
TEST_F(nst_JSONTestSuite, fail111) { shouldFail(true, 11); }
TEST_F(nst_JSONTestSuite, fail112) { shouldFail(true, 10); }
TEST_F(nst_JSONTestSuite, fail113) { shouldFail(true, 11); }
TEST_F(nst_JSONTestSuite, fail114) { shouldFail(true, 12); }
TEST_F(nst_JSONTestSuite, fail115) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail116) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail117) { shouldFail(true, 6); }
TEST_F(nst_JSONTestSuite, fail118) { shouldFail(true, 7); }
TEST_F(nst_JSONTestSuite, fail119) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail120) { shouldFail(true, 7); }
TEST_F(nst_JSONTestSuite, fail121) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail122) { shouldFail(true, 7); }
TEST_F(nst_JSONTestSuite, fail123) { shouldFail(true, 12); }
TEST_F(nst_JSONTestSuite, fail124) { shouldFail(true, 16); }
TEST_F(nst_JSONTestSuite, fail125) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail126) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail127) { shouldFail(true, 8); }
TEST_F(nst_JSONTestSuite, fail128) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail129) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail130) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail131) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail132) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail133) { shouldFail(true, 0); }
TEST_F(nst_JSONTestSuite, fail134) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail135) { shouldFail(true, 6); }
TEST_F(nst_JSONTestSuite, fail136) { shouldFail(true, 13); }
TEST_F(nst_JSONTestSuite, fail137) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail138) { shouldFail(true, 7); }
TEST_F(nst_JSONTestSuite, fail139) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, fail140) { shouldFail(true, 19); }
TEST_F(nst_JSONTestSuite, fail141) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail142) { shouldFail(true, 0); }
TEST_F(nst_JSONTestSuite, fail143) { shouldFail(true, 0); }
TEST_F(nst_JSONTestSuite, fail144) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail145) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail146) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail147) { shouldFail(true, 6); }
TEST_F(nst_JSONTestSuite, fail148) { shouldFail(true, 0); }
TEST_F(nst_JSONTestSuite, fail149) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail150) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail151) { shouldFail(true, 11); }
TEST_F(nst_JSONTestSuite, fail152) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, fail153) { shouldFail(true, 0); }
TEST_F(nst_JSONTestSuite, fail154) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, fail155) { shouldFail(true, 0); }
TEST_F(nst_JSONTestSuite, fail156) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail158) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail159) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail160) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, fail161) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail162) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail163) { shouldFail(true, 14); }
TEST_F(nst_JSONTestSuite, fail164) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail165) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail166) { shouldFail(true, 46); }
TEST_F(nst_JSONTestSuite, fail167) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, fail168) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail169) { shouldFail(true, 4); }
TEST_F(nst_JSONTestSuite, fail170) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail171) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail172) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail173) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail174) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, fail175) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail176) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, fail177) { shouldFail(true, 0); }
TEST_F(nst_JSONTestSuite, fail178) { shouldFail(true, 0); }
TEST_F(nst_JSONTestSuite, fail179) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, fail180) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail181) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, fail182) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, fail183) { shouldFail(true, 8); }
TEST_F(nst_JSONTestSuite, fail184) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, fail185) { shouldFail(true, 12); }
TEST_F(nst_JSONTestSuite, fail186) { shouldFail(true, 0); }
TEST_F(nst_JSONTestSuite, fail187) { shouldFail(true, 1); }
TEST_F(nst_JSONTestSuite, fail188) { shouldFail(true, 1); }

TEST_F(nst_JSONTestSuite, optional1) {}
TEST_F(nst_JSONTestSuite, optional2) {}
TEST_F(nst_JSONTestSuite, optional3) {}
TEST_F(nst_JSONTestSuite, optional4) {}
TEST_F(nst_JSONTestSuite, optional5) {}
TEST_F(nst_JSONTestSuite, optional6) {}
TEST_F(nst_JSONTestSuite, optional7) {}
TEST_F(nst_JSONTestSuite, optional8) {}
TEST_F(nst_JSONTestSuite, optional9) {}

TEST_F(nst_JSONTestSuite, optional10) {}
TEST_F(nst_JSONTestSuite, optional11) { shouldFail(true, 8); }
TEST_F(nst_JSONTestSuite, optional12) { shouldFail(true, 8); }
TEST_F(nst_JSONTestSuite, optional13) { shouldFail(true, 14); }
TEST_F(nst_JSONTestSuite, optional14) { shouldFail(true, 0); }
TEST_F(nst_JSONTestSuite, optional15) { shouldFail(true, 7); }
TEST_F(nst_JSONTestSuite, optional16) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, optional17) { shouldFail(true, 10); }
TEST_F(nst_JSONTestSuite, optional18) { shouldFail(true, 9); }
TEST_F(nst_JSONTestSuite, optional19) { shouldFail(true, 16); }
TEST_F(nst_JSONTestSuite, optional20) { shouldFail(true, 8); }
TEST_F(nst_JSONTestSuite, optional21) { shouldFail(true, 11); }
TEST_F(nst_JSONTestSuite, optional22) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, optional23) { shouldFail(true, 14); }
TEST_F(nst_JSONTestSuite, optional24) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, optional25) { shouldFail(true, 8); }
TEST_F(nst_JSONTestSuite, optional26) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, optional27) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, optional28) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, optional29) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, optional30) { shouldFail(true, 2); }
TEST_F(nst_JSONTestSuite, optional31) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, optional32) { shouldFail(true, 6); }
TEST_F(nst_JSONTestSuite, optional33) { shouldFail(true, 5); }
TEST_F(nst_JSONTestSuite, optional34) { setMaxDepth(501); }
TEST_F(nst_JSONTestSuite, optional35) { shouldFail(true, 0); }

TEST_F(nst_JSONTestSuite, pass1) {}
TEST_F(nst_JSONTestSuite, pass2) {}
TEST_F(nst_JSONTestSuite, pass3) {}
TEST_F(nst_JSONTestSuite, pass4) {}
TEST_F(nst_JSONTestSuite, pass5) {}
TEST_F(nst_JSONTestSuite, pass6) {}
TEST_F(nst_JSONTestSuite, pass7) {}
TEST_F(nst_JSONTestSuite, pass8) {}
TEST_F(nst_JSONTestSuite, pass9) {}

TEST_F(nst_JSONTestSuite, pass10) {}
TEST_F(nst_JSONTestSuite, pass11) {}
TEST_F(nst_JSONTestSuite, pass12) {}
TEST_F(nst_JSONTestSuite, pass13) {}
TEST_F(nst_JSONTestSuite, pass14) {}
TEST_F(nst_JSONTestSuite, pass15) {}
TEST_F(nst_JSONTestSuite, pass16) {}
TEST_F(nst_JSONTestSuite, pass17) {}
TEST_F(nst_JSONTestSuite, pass18) {}
TEST_F(nst_JSONTestSuite, pass19) {}
TEST_F(nst_JSONTestSuite, pass20) {}
TEST_F(nst_JSONTestSuite, pass21) {}
TEST_F(nst_JSONTestSuite, pass22) {}
TEST_F(nst_JSONTestSuite, pass23) {}
TEST_F(nst_JSONTestSuite, pass24) {}
TEST_F(nst_JSONTestSuite, pass25) {}
TEST_F(nst_JSONTestSuite, pass26) {}
TEST_F(nst_JSONTestSuite, pass27) {}
TEST_F(nst_JSONTestSuite, pass28) {}
TEST_F(nst_JSONTestSuite, pass29) {}
TEST_F(nst_JSONTestSuite, pass30) {}
TEST_F(nst_JSONTestSuite, pass31) {}
TEST_F(nst_JSONTestSuite, pass32) {}
TEST_F(nst_JSONTestSuite, pass33) {}
TEST_F(nst_JSONTestSuite, pass34) {}
TEST_F(nst_JSONTestSuite, pass35) {}
TEST_F(nst_JSONTestSuite, pass36) {}
TEST_F(nst_JSONTestSuite, pass37) {}
TEST_F(nst_JSONTestSuite, pass38) {}
TEST_F(nst_JSONTestSuite, pass39) {}
TEST_F(nst_JSONTestSuite, pass40) {}
TEST_F(nst_JSONTestSuite, pass41) {}
TEST_F(nst_JSONTestSuite, pass42) {}
TEST_F(nst_JSONTestSuite, pass43) {}
TEST_F(nst_JSONTestSuite, pass44) {}
TEST_F(nst_JSONTestSuite, pass45) {}
TEST_F(nst_JSONTestSuite, pass46) {}
TEST_F(nst_JSONTestSuite, pass47) {}
TEST_F(nst_JSONTestSuite, pass48) {}
TEST_F(nst_JSONTestSuite, pass49) {}
TEST_F(nst_JSONTestSuite, pass50) {}
TEST_F(nst_JSONTestSuite, pass51) {}
TEST_F(nst_JSONTestSuite, pass52) {}
TEST_F(nst_JSONTestSuite, pass53) {}
TEST_F(nst_JSONTestSuite, pass54) {}
TEST_F(nst_JSONTestSuite, pass55) {}
TEST_F(nst_JSONTestSuite, pass56) {}
TEST_F(nst_JSONTestSuite, pass57) {}
TEST_F(nst_JSONTestSuite, pass58) {}
TEST_F(nst_JSONTestSuite, pass59) {}
TEST_F(nst_JSONTestSuite, pass60) {}
TEST_F(nst_JSONTestSuite, pass61) {}
TEST_F(nst_JSONTestSuite, pass62) {}
TEST_F(nst_JSONTestSuite, pass63) {}
TEST_F(nst_JSONTestSuite, pass64) {}
TEST_F(nst_JSONTestSuite, pass65) {}
TEST_F(nst_JSONTestSuite, pass66) {}
TEST_F(nst_JSONTestSuite, pass67) {}
TEST_F(nst_JSONTestSuite, pass68) {}
TEST_F(nst_JSONTestSuite, pass69) {}
TEST_F(nst_JSONTestSuite, pass70) {}
TEST_F(nst_JSONTestSuite, pass71) {}
TEST_F(nst_JSONTestSuite, pass72) {}
TEST_F(nst_JSONTestSuite, pass73) {}
TEST_F(nst_JSONTestSuite, pass74) {}
TEST_F(nst_JSONTestSuite, pass75) {}
TEST_F(nst_JSONTestSuite, pass76) {}
TEST_F(nst_JSONTestSuite, pass77) {}
TEST_F(nst_JSONTestSuite, pass78) {}
TEST_F(nst_JSONTestSuite, pass79) {}
TEST_F(nst_JSONTestSuite, pass80) {}
TEST_F(nst_JSONTestSuite, pass81) {}
TEST_F(nst_JSONTestSuite, pass82) {}
TEST_F(nst_JSONTestSuite, pass83) {}
TEST_F(nst_JSONTestSuite, pass84) {}
TEST_F(nst_JSONTestSuite, pass85) {}
TEST_F(nst_JSONTestSuite, pass86) {}
TEST_F(nst_JSONTestSuite, pass87) {}
TEST_F(nst_JSONTestSuite, pass88) {}
TEST_F(nst_JSONTestSuite, pass89) {}
TEST_F(nst_JSONTestSuite, pass90) {}
TEST_F(nst_JSONTestSuite, pass91) {}
TEST_F(nst_JSONTestSuite, pass92) {}
TEST_F(nst_JSONTestSuite, pass93) {}
TEST_F(nst_JSONTestSuite, pass94) {}
TEST_F(nst_JSONTestSuite, pass95) {}

TEST_F(nst_JSONTestSuite, transform1) {}
TEST_F(nst_JSONTestSuite, transform2) {}
TEST_F(nst_JSONTestSuite, transform3) {}
TEST_F(nst_JSONTestSuite, transform4) {}
TEST_F(nst_JSONTestSuite, transform5) {}
TEST_F(nst_JSONTestSuite, transform6) {}
TEST_F(nst_JSONTestSuite, transform7) {}
TEST_F(nst_JSONTestSuite, transform8) {}
TEST_F(nst_JSONTestSuite, transform9) {}

TEST_F(nst_JSONTestSuite, transform10) {}
TEST_F(nst_JSONTestSuite, transform11) {}
TEST_F(nst_JSONTestSuite, transform12) { shouldFail(true, 8); }
TEST_F(nst_JSONTestSuite, transform13) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, transform14) { shouldFail(true, 14); }
TEST_F(nst_JSONTestSuite, transform15) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, transform16) { shouldFail(true, 20); }
TEST_F(nst_JSONTestSuite, transform17) { shouldFail(true, 3); }
TEST_F(nst_JSONTestSuite, transform18) {}

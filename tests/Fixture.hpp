#pragma once
#include <rili/Test.hpp>
#include <string>

class Fixture : public rili::test::TestBaseFixture {
 public:
    Fixture();
    void before() override;
    void after() override;

    void shouldFail(bool shouldFail, size_t position = 0);
    void setMaxDepth(size_t maxDepth);

 private:
    bool m_shouldFail;
    size_t m_exceptedFailurePosition;
    size_t m_maxAllowedDepth;
    std::string m_inputJson;
    std::string m_expectedJson;
};
